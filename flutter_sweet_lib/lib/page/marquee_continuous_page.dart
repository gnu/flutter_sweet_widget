import 'package:flutter/material.dart';
import 'package:flutter_sweet_lib/widget/roll_widget.dart';

class MarqueeContinuousWidgetPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("跑马灯")),
      body: Center(
        child: Container(
          height: 40.0,
          child: MarqueeContinuousWidget(
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Text("本商城于20号-30号开启促销活动")),
            duration: Duration(milliseconds: 300),
            stepOffset: 10.0,
          ),
        ),
      ),
    );
  }
}
