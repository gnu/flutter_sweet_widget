import 'package:flutter/material.dart';
import "package:pull_to_refresh/pull_to_refresh.dart";

/// 上拉加载  下拉刷新
class PullListPage extends StatefulWidget {
  @override
  _PullListPageState createState() => _PullListPageState();
}

class _PullListPageState extends State<PullListPage> {
  final RefreshController _refreshController = RefreshController();
  final List<String> _dataList = List.generate(10, (i) => i.toString());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("count: ${_dataList.length}")),
      body: RefreshIndicator(
          child: SmartRefresher(
            enablePullDown: false,
            enablePullUp: true,
            enableOverScroll: false,
            footerBuilder: (context, mode) {
              return ClassicIndicator(mode: mode);
            },
            footerConfig: LoadConfig(triggerDistance: 30.0),
            controller: _refreshController,
            onRefresh: (up) {
              if(_dataList.length > 30) {
                _refreshController.sendBack(false, RefreshStatus.noMore);
              }

              Future.delayed(const Duration(seconds: 2), () {
                setState(() {
                  _dataList
                      .addAll(List.generate(3, (i) => 'refresh: ${i.toString()}'));
                });
              }).then((val) {
                _refreshController.sendBack(false, RefreshStatus.idle);
              });
            },
            onOffsetChange: (bool up, double offset) {
              print("$up:$offset");
            },
            child: ListView.builder(
                itemBuilder: (context, index) => ListTile(
                      title: Text(_dataList[index]),
                    ),
                itemCount: _dataList.length),
          ),
          onRefresh: () {
            return Future.delayed(const Duration(seconds: 2), () {
              setState(() {
                _dataList.insertAll(
                    0, List.generate(4, (i) => 'onRefresh: ${i.toString()}'));
              });
            });
          }),
    );
  }
}
