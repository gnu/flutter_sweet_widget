import 'package:flutter/material.dart';

class ManyListViewPage extends StatelessWidget {
  final List<List<String>> _tabs = [
    List.generate(20, (i) => "1-${i.toString()}"),
    List.generate(20, (i) => "2-${i.toString()}"),
    List.generate(20, (i) => "3-${i.toString()}"),
    List.generate(20, (i) => "4-${i.toString()}"),
    List.generate(20, (i) => "5-${i.toString()}"),
  ];

  @override
  Widget build(BuildContext context) {
    return Material(
      child: CustomScrollView(
        slivers: <Widget>[
          SliverPadding(
            padding: const EdgeInsets.all(8.0),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                return Column(
                  children: _tabs[index]
                      .map((i) => ListTile(title: Text(i)))
                      .toList(),
                );
              }, childCount: _tabs.length),
            ),
          ),
        ],
      ),
    );
  }
}
