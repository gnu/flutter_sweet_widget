import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldPage extends StatefulWidget {
  @override
  _TextFieldPageState createState() => _TextFieldPageState();
}

class _TextFieldPageState extends State<TextFieldPage> {
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("TextField")),
      body: TextField(
        controller: _controller,
        enabled: true,
        inputFormatters: [
          BlacklistingTextInputFormatter("like", replacementString: "xxx"),
        ],
      ),
    );
  }
}
