import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class SwiperDemoPage extends StatefulWidget {
  @override
  _SwiperDemoPageState createState() => _SwiperDemoPageState();
}

class _SwiperDemoPageState extends State<SwiperDemoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("轮播图")),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 188.0,
              child: Swiper(
                itemBuilder: (BuildContext context, int index) {
                  return Image.network(
                    "http://via.placeholder.com/288x188",
                    fit: BoxFit.fill,
                  );
                },
                itemCount: 3,
                viewportFraction: 0.8,
                scale: 0.9,
              ),
            ),
            const SizedBox(height: 20.0),
            Container(
              height: 150.0,
              child: Swiper(
                autoplay: true,
                itemBuilder: (BuildContext context, int index) {
                  return Image.network(
                    "http://via.placeholder.com/350x150",
                    fit: BoxFit.fill,
                  );
                },
                itemCount: 3,
                pagination: SwiperPagination(), // 轮播指示器是否显示
                control: SwiperControl(), // 方向控制器是否显示
              ),
            ),
          ],
        ),
      ),
    );
  }
}
