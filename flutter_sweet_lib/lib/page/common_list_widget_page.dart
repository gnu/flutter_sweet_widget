import 'package:flutter/material.dart';
import 'package:flutter_sweet_lib/widget/common_list_widget.dart';
class CommonListWidgetPage extends StatefulWidget {
  @override
  _CommonListWidgetPageState createState() => _CommonListWidgetPageState();
}

class _CommonListWidgetPageState extends State<CommonListWidgetPage> {
  List<String> _data = List.generate(10, (i) => i.toString());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("CommonList len: ${_data.length}")),
      body: CommonListWidget<String>(
        data: _data,
        height: 40.0,
        itemWidgetBuilder: (context, index, item){
          return Container(
            height: 40.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(item),
                Divider(height: 1.0),
              ],
            ),
          );
        },
        onLoadMore: () async {
          if(_data.length >= 30) {
            return false;
          }
          final _d2 = List.generate(4, (i) => i.toString());
          await Future.delayed(Duration(seconds: 2));
          setState(() {
            _data.addAll(_d2);
          });
        },
        onRefresh: () async {
          final _d2 = List.generate(4, (i) => i.toString());
          await Future.delayed(Duration(seconds: 1));
          setState(() {
            _data.insertAll(0, _d2);
          });
        },
      ),
    );
  }
}
