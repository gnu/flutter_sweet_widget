import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_sweet_lib/project_demo_page/youpin_first_page.dart';

class TabBarViewPage extends StatefulWidget {
  @override
  _TabBarViewPageState createState() => _TabBarViewPageState();
}

class _TabBarViewPageState extends State<TabBarViewPage>
    with SingleTickerProviderStateMixin {
  List<TypeModel> _typeList = [
    TypeModel(no: 0, typeName: '推荐'),
    TypeModel(no: 1, typeName: '手机'),
    TypeModel(no: 2, typeName: '家电'),
    TypeModel(no: 3, typeName: '居家'),
    TypeModel(no: 4, typeName: '智能'),
    TypeModel(no: 5, typeName: '饮食'),
    TypeModel(no: 6, typeName: '餐厨'),
    TypeModel(no: 7, typeName: '服饰'),
    TypeModel(no: 8, typeName: '鞋包'),
    TypeModel(no: 9, typeName: '笔记本'),
    TypeModel(no: 10, typeName: '影音'),
    TypeModel(no: 11, typeName: '健康'),
    TypeModel(no: 12, typeName: '出行'),
    TypeModel(no: 13, typeName: '洗护'),
    TypeModel(no: 14, typeName: '日杂'),
    TypeModel(no: 15, typeName: '母婴'),
    TypeModel(no: 16, typeName: '配件'),
    TypeModel(no: 17, typeName: '品牌'),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      title: Text("hhh"),
      actions: <Widget>[
        IconButton(
            icon: Icon(Icons.chevron_right),
            onPressed: () {
              int index = Random().nextInt(_typeList.length);
              print("${_typeList[index].typeName}");
            })
      ],
      bottom: TabBar(
          isScrollable: true,
          tabs: _typeList.map((model) {
            return Tab(text: model.typeName);
          }).toList()),
    );
    return DefaultTabController(
      length: _typeList.length,
      child: Scaffold(
        appBar: appBar,
        body: TabBarView(
            children: _typeList.map((model) {
//          return Center(
//            child: Text(model.typeName, style: TextStyle(fontSize: 28.0)),
//          );
            return MyTabBarView();
        }).toList()),
      ),
    );
  }
}

class MyTabBarView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(height: 100.0, color: Colors.red),
          Container(height: 100.0, color: Colors.yellowAccent),
          Container(height: 100.0, color: Colors.blueAccent),
          Container(height: 100.0, color: Colors.blueGrey),
          Container(height: 100.0, color: Colors.grey),
          Container(height: 100.0, color: Colors.red),
          Container(height: 100.0, color: Colors.yellowAccent),
        ],
      ),
    );
  }
}
