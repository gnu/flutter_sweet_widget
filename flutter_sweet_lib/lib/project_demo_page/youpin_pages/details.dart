import 'package:flutter/material.dart';
import 'package:flutter_sweet_lib/project_demo_page/youpin_pages/goods_details_page.dart';

class DetailsPage extends StatefulWidget {
  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  ScrollController _controller = ScrollController();
  bool _showTitle = false;

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      if (_controller.offset >= 180) {
        setState(() {
          _showTitle = true;
        });
      }
      if (_controller.offset == 0.0) {
        setState(() {
          _showTitle = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        controller: _controller,
        slivers: <Widget>[
          SliverAppBar(
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                color: Colors.orange[300],
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    CircleAvatar(
                      radius: 35.0,
                      backgroundColor: Colors.white70,
                    ),
                    const SizedBox(width: 15.0),
                    Text("Sweet",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold)),
                    Expanded(
                        child: Align(
                      alignment: Alignment.centerRight,
                      child: Icon(
                        Icons.chevron_right,
                        color: Colors.white,
                      ),
                    ))
                  ],
                ),
              ),
            ),
            leading: Container(width: 0.0, height: 0.0),
            pinned: true,
            elevation: 0.5,
            expandedHeight: 160.0,
            backgroundColor: Colors.white,
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.settings,
                      color: _showTitle ? Colors.grey : null),
                  onPressed: () {}),
              IconButton(
                  icon: Icon(Icons.notifications_none,
                      color: _showTitle ? Colors.grey : null),
                  onPressed: () {}),
            ],
          ),
          SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
            Widget _widget = Container(width: 0.0, height: 0.0);
            switch (index) {
              case 0:
                _widget = OrderCardWidget();
                break;
              case 1:
                _widget = const AdWidget();
                break;
              case 2:
                _widget = _OtherWidget();
                break;
              case 3:
                _widget = Divider(height: 1.0);
                break;
              case 4:
                _widget = _BottomWidget();
                break;
              case 5:
                _widget = Container(
                  height: 10.0,
                  color: Colors.grey[200],
                );
                break;
              case 6:
                _widget = Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Text("专属推荐",
                      style: TextStyle(
                          fontSize: 17.0, fontWeight: FontWeight.bold)),
                );
            }
            return _widget;
          }, childCount: 10)),
          _RecommendList(),
        ],
      ),
    );
  }
}

class _RecommendList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.all(10.0),
      sliver: SliverGrid(
        //Grid
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, //Grid按两列显示
          mainAxisSpacing: 10.0,
          crossAxisSpacing: 10.0,
          childAspectRatio: 1.0,
        ),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            //创建子widget
            return Container(
              color: Colors.cyan[100 * ((index+1) % 9)]
            );
          },
          childCount: 10,
        ),
      ),
    );
  }
}

class _BottomWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget w = Wrap(
      spacing: 20.0,
      runSpacing: 30.0,
      alignment: WrapAlignment.start,
      crossAxisAlignment: WrapCrossAlignment.end,
      children: <Widget>[
        TitleButton(
          icon: Icon(Icons.monetization_on, size: 28.0, color: bodyColor),
          onTap: () {},
          title: "我的资产",
          sizedBox: const SizedBox(height: 3.0),
        ),
        TitleButton(
          icon: Icon(Icons.location_on, size: 28.0, color: bodyColor),
          onTap: () {},
          title: "地址管理",
          sizedBox: const SizedBox(height: 3.0),
        ),
        TitleButton(
          icon: Icon(Icons.help, size: 28.0, color: bodyColor),
          onTap: () {},
          title: "帮助中心",
          sizedBox: const SizedBox(height: 3.0),
        ),
        TitleButton(
          icon:
              Icon(Icons.account_balance_wallet, size: 28.0, color: bodyColor),
          onTap: () {},
          title: "协议规则",
          sizedBox: const SizedBox(height: 3.0),
        ),
        TitleButton(
          icon: Icon(Icons.photo, size: 28.0, color: bodyColor),
          onTap: () {},
          title: "资质证照",
          sizedBox: const SizedBox(height: 3.0),
        ),
        TitleButton(
          icon: Icon(Icons.monetization_on, size: 28.0, color: bodyColor),
          onTap: () {},
          title: "小米贷款",
          sizedBox: const SizedBox(height: 3.0),
        ),
      ],
    );
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
      child: w,
    );
  }
}

class _OtherWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
      child: Wrap(
        spacing: 20.0,
        alignment: WrapAlignment.start,
        crossAxisAlignment: WrapCrossAlignment.end,
        children: <Widget>[
          TitleButton(
            icon: Icon(Icons.add_to_home_screen, size: 28.0, color: bodyColor),
            onTap: () {},
            title: "拼团订单",
            sizedBox: const SizedBox(height: 3.0),
          ),
          TitleButton(
            icon: Icon(Icons.stars, size: 28.0, color: bodyColor),
            onTap: () {},
            title: "收藏",
            sizedBox: const SizedBox(height: 3.0),
          ),
          TitleButton(
            icon: Icon(Icons.monetization_on, size: 28.0, color: bodyColor),
            onTap: () {},
            title: "优惠券",
            sizedBox: const SizedBox(height: 3.0),
          ),
          TitleButton(
            icon: Icon(Icons.visibility, size: 28.0, color: bodyColor),
            onTap: () {},
            title: "新人邀请",
            sizedBox: const SizedBox(height: 3.0),
          ),
        ],
      ),
    );
  }
}

class AdWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: 70.0,
      color: Colors.grey[200],
      child: Text("广告位"),
    );
  }

  const AdWidget();
}

class OrderCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Card(
        child: Container(
          alignment: Alignment.center,
          child: Column(
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("我的订单"),
                    AllOrderButton(onTap: () {}),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    TitleButton(
                        icon: Icon(Icons.map, color: Colors.redAccent),
                        title: "待付款",
                        onTap: () {},
                        sizedBox: const SizedBox(height: 7.0)),
                    TitleButton(
                        icon: Icon(Icons.print, color: Colors.orange),
                        title: "待收货",
                        onTap: () {},
                        sizedBox: const SizedBox(height: 7.0)),
                    TitleButton(
                        icon: Icon(Icons.account_balance, color: Colors.green),
                        title: "待评价",
                        onTap: () {},
                        sizedBox: const SizedBox(height: 7.0)),
                    TitleButton(
                        icon: Icon(Icons.credit_card, color: Colors.blue),
                        title: "退款/售后",
                        onTap: () {},
                        sizedBox: const SizedBox(height: 7.0)),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class TitleButton extends StatelessWidget {
  final Icon icon;
  final String title;
  final VoidCallback onTap;
  final SizedBox sizedBox;

  const TitleButton({this.icon, this.title, this.onTap, this.sizedBox});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: 70.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            icon,
            sizedBox,
            Text(title, style: TextStyle(fontSize: 13.0))
          ],
        ),
      ),
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
    );
  }
}

class AllOrderButton extends StatelessWidget {
  final VoidCallback onTap;

  const AllOrderButton({this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Row(children: <Widget>[
        Text("全部订单", style: TextStyle(fontSize: 13.0, color: Colors.grey)),
        Icon(Icons.chevron_right, size: 13.0, color: Colors.grey),
      ], crossAxisAlignment: CrossAxisAlignment.center),
    );
  }
}
