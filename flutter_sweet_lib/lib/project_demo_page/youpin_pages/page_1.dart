import 'package:flutter/material.dart';
import 'package:flutter_sweet_lib/project_demo_page/youpin_first_page.dart';
import 'package:flutter_sweet_lib/project_demo_page/youpin_pages/goods_details_page.dart';

class Recommend2Page extends StatefulWidget {
  final ClassifyModel model;

  const Recommend2Page({this.model});

  @override
  _Recommend2PageState createState() => _Recommend2PageState();
}

class _Recommend2PageState extends State<Recommend2Page> {
  Map<String, List<String>> _goodsMap = {};

  @override
  void initState() {
    super.initState();
    widget.model.list.forEach((name) {
      final list = List.generate(10, (i) => "$name - $i");
      _goodsMap[name] = list;
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final width = mediaQuery.size.width / 2;
    final tabBar = TabBar(
        isScrollable: true,
        tabs: widget.model.list
            .map(
              (name) => Tab(
                    child: Text(name,
                        style: TextStyle(
                            color: Theme.of(context).textTheme.title.color)),
                  ),
            )
            .toList());
    return DefaultTabController(
        length: widget.model.list.length,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios, color: Colors.grey[500]),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            backgroundColor: Colors.white,
            title: Text(widget.model.title,
                style: TextStyle(color: Colors.black, fontSize: 17.0)),
            centerTitle: true,
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.search, color: Colors.grey[500]),
                onPressed: () {},
              )
            ],
            bottom: tabBar,
          ),
          body: TabBarView(
              children: widget.model.list.map((name) {
            return RefreshIndicator(
                child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                    itemBuilder: (context, index) {
                      final goodsName = _goodsMap[name][index];
                      return GestureDetector(
                        onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) =>
                                    GoodsDetailsPage(goodsName))),
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.center,
                                width: width,
                                height: 120.0,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  color: Colors.grey[200],
                                ),
                                child: Text(goodsName),
                              ),
//                            Container(
//                              child: Text("11小时长续航，轻至13.6克", style: TextStyle(fontSize: 12.0)),
//                              color: Colors.orange[100],
//                              width: width,
//                              height: 10.0,
//                            ),
                              Text(goodsName),
                              Text(
                                "￥99",
                                style: TextStyle(color: Colors.red[700]),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    itemCount: 10),
                onRefresh: () async {
                  await Future.delayed(Duration(seconds: 2), () {});
                });
          }).toList()),
        ));
  }
}
