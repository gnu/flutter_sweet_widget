import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class RecommendPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          MySwiper(height: 177.0),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: HotTypeWidget(
                  list: ["每日新品", "小米众筹", "限时抢购", "热销榜单", "随便逛逛"])),
          const Hot2TypeWidget(list: [
            Hot2TypeModel(title: "2月迎春好物榜", subTitle: "减脂神器 最高直降500元"),
            Hot2TypeModel(title: "星动潮搭开学季", subTitle: "让你赚足回头率"),
            Hot2TypeModel(title: "紫米7周年庆", subTitle: "爆品限时低至6折"),
            Hot2TypeModel(title: "家电换新季", subTitle: "爆款最高立省400元"),
          ]),
          Padding(padding: EdgeInsets.all(10.0), child: CrowdWidget()),
        ],
      ),
    );
  }
}

class CrowdWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("小米众筹",
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0)),
              Row(
                children: <Widget>[
                  Text("更多", style: TextStyle(color: Colors.grey)),
                  Icon(Icons.chevron_right, color: Colors.grey),
                ],
              )
            ],
          ),
          const SizedBox(height: 10.0),
          Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: 170.0,
                foregroundDecoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                  color: Colors.grey[200],
                ),
              ),
              Positioned(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("睡眠宝精油香氛仪"),
                        Text("天然精油，舒压好眠，快速扩香",
                            style:
                                TextStyle(fontSize: 12.0, color: Colors.grey)),
                        Text("￥99", style: TextStyle(color: Colors.red[700])),
                      ]),
                  top: 12.0,
                  left: 12.0),
              Positioned(
                bottom: 5.0,
                left: 5.0,
                child: Text("1305/2000人支持",
                    style: TextStyle(color: Colors.grey, fontSize: 12.0)),
              ),
              Positioned(
                  bottom: 5.0,
                  right: 5.0,
                  child: Text("65%", style: TextStyle(color: Colors.red[700]))),
            ],
          ),
          SizedBox(
            child: LinearProgressIndicator(
              value: 0.65,
              backgroundColor: Colors.grey[300],
              valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
            ),
            height: 4.0,
          )
        ],
      ),
    );
  }
}

class Hot2TypeWidget extends StatelessWidget {
  final List<Hot2TypeModel> list;

  const Hot2TypeWidget({this.list});

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 10.0,
      runSpacing: 10.0,
      children: list.map((model) {
        return Container(
          color: Colors.grey[200],
          width: 170.0,
          height: 150.0,
          child: Column(
            children: <Widget>[
              Text(model.title),
              Text(model.subTitle,
                  style: TextStyle(color: Colors.grey, fontSize: 12.0)),
            ],
          ),
        );
      }).toList(),
    );
  }
}

class Hot2TypeModel {
  final String title;
  final String subTitle;

  const Hot2TypeModel({this.title, this.subTitle});
}

class HotTypeWidget extends StatelessWidget {
  final List<String> list;

  const HotTypeWidget({this.list});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: list.map((i) {
        return Container(
            alignment: Alignment.bottomCenter,
            height: 70.0,
            child: Text(i),
            color: Colors.grey[200]);
      }).toList(),
    );
  }
}

class MySwiper extends StatelessWidget {
  final double height;

  const MySwiper({this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          return Container(
            alignment: Alignment.center,
            height: height,
            child: Text("📱 $index", style: TextStyle(fontSize: 28.0)),
            color: Colors.grey[200],
          );
        },
        autoplay: true,
        autoplayDelay: 4000,
        itemCount: 3,
        pagination: SwiperPagination(),
        scale: 0.9,
      ),
    );
  }
}
