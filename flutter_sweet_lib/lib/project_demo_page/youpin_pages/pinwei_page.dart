import 'package:flutter/material.dart';
import 'package:flutter_sweet_lib/project_demo_page/youpin_pages/goods_details_page.dart';
import 'package:flutter_sweet_lib/project_demo_page/youpin_pages/page_0.dart';

class PinWeiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
              title: Text("品味", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: Colors.white,
              leading: Container(width: 0.0, height: 0.0)),
          SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
            Widget _w = Container(
              child: Text("Hello"),
            );
            switch (index) {
              case 0:
                _w = MySwiper(height: 177.0);
                break;
              case 1:
                _w = _TagList();
                break;
              case 2:
                _w = Container(
                  height: 10.0,
                  color: Colors.grey[200],
                );
                break;
              case 3:
                _w = _AdWidget();
                break;
              case 4:
                _w = _PinWeiGoodsList();
                break;
              case 5:
                _w = Container(
                  height: 10.0,
                  color: Colors.grey[200],
                );
                break;
              case 6:
                _w = Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("独家专栏"),
                      Icon(Icons.chevron_right, color: Colors.grey)
                    ],
                  ),
                );
                break;
            }
            return _w;
          }, childCount: 7)),
          SliverPadding(
            padding: const EdgeInsets.all(10.0),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate(
                  (context, index) => _ArticleItem(index),
                  childCount: 10),
            ),
          )
        ],
      ),
    );
  }
}

class _ArticleItem extends StatelessWidget {
  final int index;

  _ArticleItem(this.index);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            height: 100.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                color: Colors.lightGreen[100 * ((index + 1) % 20)]),
          ),
          const SizedBox(height: 5.0),
          Text("标题${index}"),
          Text("副标题${index}",
              style: TextStyle(fontSize: 12.0, color: Colors.grey)),
          const SizedBox(height: 5.0),
          FlatButton(
            onPressed: () {},
            color: bodyColor,
            shape: RoundedRectangleBorder(),
            child: Text("去发现", style: TextStyle(color: Colors.white)),
          )
        ],
      ),
    );
  }
}

class _PinWeiGoodsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10.0, top: 10.0, bottom: 10.0),
      height: 120.0,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => Container(
                color: Colors.blue[100 * ((index + 1) % 9)],
                margin: EdgeInsets.symmetric(horizontal: 5.0),
              ),
          itemExtent: 120,
          itemCount: 8),
    );
  }
}

class _AdWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("品味周刊"),
                  Icon(Icons.chevron_right, color: Colors.grey, size: 18.0)
                ]),
          ),
          const SizedBox(height: 10.0),
          Container(
            alignment: Alignment.center,
            height: 100.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                color: Colors.grey[300]),
          )
        ],
      ),
    );
  }
}

class _TagList extends StatelessWidget {
  final List<String> _list = ["本周值得买", "美学实验室", "新品体验馆", "不可能研究所", "发现好货"];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: ListView.builder(
          itemBuilder: (context, index) => Container(
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                        radius: 30.0,
                        backgroundColor: Colors.green[100 * ((index + 1) % 9)]),
                    const SizedBox(height: 3.0),
                    Text("${_list[index]}",
                        style: TextStyle(fontSize: 12.0, color: Colors.black)),
                  ],
                ),
              ),
          itemCount: _list.length,
          itemExtent: 80.0,
          scrollDirection: Axis.horizontal),
      height: 100.0,
    );
  }
}
