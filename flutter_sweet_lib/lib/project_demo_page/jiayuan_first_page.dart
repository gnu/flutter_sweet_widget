import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_sweet_lib/bloc/bloc_provider.dart';
import 'package:flutter_sweet_lib/bloc/jiayuan/jiayuan_application_bloc.dart';
import 'package:flutter_sweet_lib/widget/roll_widget.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class JiaYuanFirstPage extends StatefulWidget {
  @override
  _JiaYuanFirstPageState createState() => _JiaYuanFirstPageState();
}

class _JiaYuanFirstPageState extends State<JiaYuanFirstPage> {
  int _currentIndex = 0;
  final PageController _pageController = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AppTitle(title: "游客小区", onTap: () {}),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: () {})
        ],
      ),
      body: PageView(
        controller: _pageController,
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SwiperWidget(height: 160.0),
                NotifyWidget(),
                Padding(
                    child: FunctionWidget(),
                    padding: EdgeInsets.symmetric(vertical: 5.0)),
                SwiperWidget(height: 130.0),
              ],
            ),
          ),
          Center(child: Text("开门")),
          Center(child: Text("我的")),
        ],
        onPageChanged: _onChange,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text("主页")),
          BottomNavigationBarItem(icon: Icon(Icons.apps), title: Text("开门")),
          BottomNavigationBarItem(icon: Icon(Icons.person), title: Text("我的")),
        ],
        currentIndex: _currentIndex,
        onTap: _onChange,
      ),
    );
  }

  void _onChange(int index) {
    setState(() {
      _currentIndex = index;
      _pageController.animateToPage(index,
          duration: Duration(milliseconds: 10), curve: Curves.ease);
    });
  }
}

class NotifyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            IconButton(
                icon: Icon(Icons.notifications_active, color: Colors.blue),
                onPressed: () {}),
            Container(
              height: 40.0,
              width: MediaQuery.of(context).size.width -
                  IconTheme.of(context).size -
                  50,
              child: MarqueeContinuousWidget(
                child: Center(child: Text("本商城于20号-30号开启促销活动               ")),
                duration: Duration(milliseconds: 300),
                stepOffset: 10.0,
              ),
            ),
          ],
        ),
        const Divider(height: 1.0)
      ],
    );
  }
}

class FunctionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: List.generate(
        8,
        (i) => FunctionItemWidget(
              icon: IconButton(
                icon: Icon(Icons.apps),
                onPressed: () {},
              ),
              name: '模块${i.toString()}',
            ),
      ),
    );
  }
}

class FunctionItemWidget extends StatelessWidget {
  final IconButton icon;
  final String name;

  FunctionItemWidget({this.icon, this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 75.0,
      height: 75.0,
      child: Column(
        children: <Widget>[
          icon,
          Text(name, overflow: TextOverflow.ellipsis),
        ],
      ),
    );
  }
}

class SwiperWidget extends StatelessWidget {
  final double height;

  SwiperWidget({this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Swiper(
        autoplay: true,
        autoplayDelay: 2000,
        itemBuilder: (BuildContext context, int index) {
          return Image.network(
            "http://via.placeholder.com/350x${height.toInt()}",
            fit: BoxFit.fill,
          );
        },
        itemCount: 3,
        pagination: SwiperPagination(), // 轮播指示器是否显示
//        control: SwiperControl(), // 方向控制器是否显示
      ),
    );
  }
}

class AppTitle extends StatelessWidget {
  final String title;

  final VoidCallback onTap;

  AppTitle({this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.location_on),
          const SizedBox(width: 5.0),
          Text(title, overflow: TextOverflow.ellipsis),
          Icon(Icons.arrow_drop_down)
        ],
      ),
    );
  }
}
