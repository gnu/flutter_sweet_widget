import 'package:flutter/material.dart';

typedef ItemWidgetBuilder<T> = Widget Function(
    BuildContext context, int index, T t);
typedef OnLoadMore = Future<bool> Function();

/// 下拉刷新 上拉加载更多的 list 组件
///
class CommonListWidget<T> extends StatelessWidget {
  final RefreshCallback onRefresh;
  final OnLoadMore onLoadMore;
  final ItemWidgetBuilder<T> itemWidgetBuilder;
  final List<T> data;

  final double height;

  final Widget loadingWidget;
  final Widget noDataWidget;
  final Widget emptyWidget;

  const CommonListWidget(
      {@required this.data,
      @required this.itemWidgetBuilder,
      @required this.onRefresh,
      @required this.onLoadMore,
      @required this.height,
      this.emptyWidget,
      this.loadingWidget,
      this.noDataWidget});

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context);
    final _loadingWidget =
        loadingWidget == null ? LoadingWidget(null, height) : loadingWidget;
    final _emptyWidget =
        emptyWidget == null ? Container(width: 0.0, height: 0.0) : emptyWidget;
    final _noDataWidget =
        noDataWidget == null ? LoadingWidget("没有更多数据了", height) : noDataWidget;

    print(
        "屏幕高度：${media.size.height}, 组件占用：${data.length * height}, appbar: ${kToolbarHeight}");
    return RefreshIndicator(
        child: ListView.builder(
            itemExtent: height,
            physics: const AlwaysScrollableScrollPhysics(),
            itemCount: data.length + 1,
            itemBuilder: (context, index) {
              if (index == data.length) {
                if ((data.length * height) >= (media.size.height - kToolbarHeight - 20)) {
                  return FutureBuilder<bool>(
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return snapshot.data ? _loadingWidget : _noDataWidget;
                        } else {
                          return _loadingWidget;
                        }
                      },
                      future: onLoadMore());
                } else {
                  return data.isNotEmpty
                      ? Container(width: 0.0, height: 0.0)
                      : _emptyWidget;
                }
              } else {
                final _item = data[index];
                return itemWidgetBuilder(context, index, _item);
              }
            }),
        onRefresh: onRefresh);
  }
}

class LoadingWidget extends StatelessWidget {
  final String noData;
  final double height;

  const LoadingWidget(this.noData, this.height);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Offstage(
                offstage: noData != null,
                child: SizedBox(
                    width: 15.0,
                    height: 15.0,
                    child: CircularProgressIndicator(strokeWidth: 2.0)),
              ),
              Text(noData != null ? noData : "  正在加载...")
            ]),
        height: height);
  }
}
