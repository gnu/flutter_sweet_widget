import 'package:collection/collection.dart';

void main() {
//  var d = Demo("hh");
//  print(d?.name);
//
//  d = null;
//  d ??= Demo("x1");
//  print(d.toString());
//
//  d = null;
//  print(d ?? Demo("x2"));
//
//  Future.delayed(new Duration(seconds: 2), () {
//    //return "hi world!";
//    throw AssertionError("Error");
//  }).then((data) {
//    //执行成功会走到这里
//    print(data);
//  }).catchError((e, stack) {
//    //执行失败会走到这里
//    print(e);
//    print("------------");
//    print(stack);
//    print("------------");
//  }).whenComplete(() {
//    //无论成功或失败都会走到这里
//    print("when complete");
//  });

//  final _list = <String>["h", "a", "c", "d"];
//  _list.insertAll(1, ["aa"]);
//  print(_list);
  var _list = <CartModel0>[
    CartModel0(299.00, 1, false),
    CartModel0(109.00, 1, false),
    CartModel0(7599.00, 1, false),
  ];
  var sum = _list
      .where((i) => i.selected)
      .map((m) => m.num * m.price)
      .fold(0.0, (a, b) => a + b);
  print(sum);
}

class CartModel0 {
  final bool selected;
  final double price;
  final int num;

  CartModel0(this.price, this.num, this.selected);
}

class Demo {
  final String name;

  Demo(this.name);

  @override
  String toString() {
    return 'D{name: $name}';
  }
}
