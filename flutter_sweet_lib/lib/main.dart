import 'package:flutter/material.dart';
import 'package:flutter_sweet_lib/bloc/bloc_provider.dart';
import 'package:flutter_sweet_lib/bloc/jiayuan/jiayuan_application_bloc.dart';
import 'package:flutter_sweet_lib/page/many_list_view_page.dart';
import 'package:flutter_sweet_lib/page/marquee_continuous_page.dart';
import 'package:flutter_sweet_lib/page/pull_list_page.dart';
import 'package:flutter_sweet_lib/page/stream_builder_page.dart';
import 'package:flutter_sweet_lib/page/swiper_demo_page.dart';
import 'package:flutter_sweet_lib/page/tab_bar_view_page.dart';
import 'package:flutter_sweet_lib/page/text_field_page.dart';
import 'package:flutter_sweet_lib/project_demo_page/jiayuan_first_page.dart';
import 'package:flutter_sweet_lib/project_demo_page/youpin_first_page.dart';
import 'package:flutter_sweet_lib/project_demo_page/youpin_pages/goods_details_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: bodyColor,
        primarySwatch: Colors.grey
      ),
      routes: {
        "/PullListPage": (context) => PullListPage(),
        "/MarqueeContinuousWidget": (context) => MarqueeContinuousWidgetPage(),
        "/StreamBuilderDemoPage": (context) => StreamBuilderDemoPage(),
        "/TextFieldPage": (context) => TextFieldPage(),
        "/SwiperDemoPage": (context) => SwiperDemoPage(),
        "/JiaYuan": (context) => BlocProvider(
              child: JiaYuanFirstPage(),
              bloc: JYApplicationBloc(),
            ),
        "/YouPinAllPage" : (context) => YouPinAllPage(),
        "/TabBarViewPage" : (context) => TabBarViewPage(),
        "/ManyListViewPage" : (context) => ManyListViewPage(),
      },
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  final List<PageModel> _list = [
    PageModel("PullListPage", "/PullListPage"),
    PageModel("跑马灯效果", "/MarqueeContinuousWidget"),
    PageModel("StreamBuilder", "/StreamBuilderDemoPage"),
    PageModel("TextFieldPage", "/TextFieldPage"),
    PageModel("轮播图", "/SwiperDemoPage"),
    PageModel("JiaYuan", "/JiaYuan"),
    PageModel("小米有品页面", "/YouPinAllPage"),
    PageModel("TabBarView", "/TabBarViewPage"),
    PageModel("ManyListView", "/ManyListViewPage"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (context, index) => ListTile(
              title: Text(_list[index].name),
              onTap: () => Navigator.of(context).pushNamed(_list[index].router),
            ),
        itemCount: _list.length,
      ),
    );
  }
}

class PageModel {
  final String name;
  final String router;
  final bool topFlag;
  PageModel(this.name, this.router, {this.topFlag = false});
}
